# Calculator: The Game - GTK port

Based on the popular iOS/Android game: "Calculator: The Game"
Credit goes to the original creators -- written using PyGTK

![Gameplay gif](https://gitlab.com/joyod3/Calc/raw/master/media/Calc-1.gif)

## Running

The game is almost done - but you can run it as-is by:

* `./install.sh`

* `calc` or launch from desktop

This will startup the GTK frontend and load up the first level.
Future levels are yet to be implemented, and will be loaded from
a single json file (levels.json)

## Contribting

We need level creators!! You can add a level by modifying the levels.json file
and sending pull requests. The level format is simple, and as follows:

A level is a json object that contains:

* A 2D array of buttons (3 vertical rows)

* The amount of moves allowed

* The end goal

* The starting number

The buttons array should contain three arrays of buttons. The buttons are
populated from the top-down, and each button should contain a method and value.

For example:
The first row is usually empty; the second row contains normal actions like add
multiply, subtract, divide and append; and the last row normally only contains the
reset button. Here's what the last row should look like:

`[ {"method": "clr", "value": "N/A"}, {"method": "blank", "value": "N/A"}, {"method": "blank", "value": "N/A"} ]`

This would populate the last row as:

`CLR`  
` `  
` `  

You can just copy a level and modify the data in the levels file.

The methods available so far are as follows:

* Multiply - Multiply the current number by value

  * `{"method": "mult", "value": 9}` -- Multiplies current number by `9`

* Add - Adds the value to the current number

  * `{"method": "add", "value": 2}` -- Adds `2` to the current number

* Divide - Divides the current number by the given value

  * `{"method": "div", "value": 2}` -- Divides the current number by `2`

* Subtract - Subtracts the given value from the current number

  * `{"method": "sub", "value": 5}` -- Subracts `5` from the current number

* Append - Adds the given value to the end of the current number

  * `{"method": "append", "value": 10}` -- Appends `10` to the end of the current number

* Sum - Adds all digits of the current number together

  * `{"method": "sum", "value": "N/A"}` -- If the current number is `15` it would become `6`

* Replace - Replaces the first given number by the second given number

  * `{"method": "replace", "value": "15 0"}` -- Replaces all instances of `15` with `0` so `915` becomes `90`

* Reverse - Reverses current number

  * `{"method": "reverse", "value": "N/A"}` -- Flips current number so `915` becomes `519`

* Delete - Deletes last digit of current number

  * `{"method": "delete", "value": "N/A"}` -- Deletes last digit of current number so `6312` becomes `631`