class Level():
    def __init__(self, buttons, moves, goal, starting, ui):
        ## A button is a dict object with the following format:
        # ["method_name", "args/0"]
        # ie. ["replace", "44 43"], ["CLR", "0"]
        self.original = starting
        self.original_moves = moves
        self.buttons = buttons
        self.moves = moves
        self.goal = goal
        self.current = starting
        self.ui = ui

    ''' This method will return 0 unless the self.current buffer is equal to 
    the target number '''
    def decrease_moves(self):
        # Check for success
        if self.current == self.goal:
            # TODO: End game and show gtk alert #
            self.moves -= 1
            self.ui.update()
            self.ui.win()
        elif self.moves == 1 and not self.current == self.goal:
            self.moves -= 1
            self.ui.update()
            self.ui.lose()
        else:
            # Decrease moves and return 0
            self.moves -= 1
            self.ui.update()

    ## Basic actions - 
    # CLR
    def CLR(self, button):
        self.current = self.original
        self.moves = self.original_moves
        self.ui.update()

    ## Common actions = 
    # MULT, ADD, SUBTRACT, DIVIDE, APPEND
    
    ''' Multiplies self.current by the given number '''
    def mult(self, button, num):
        self.current *= num
        self.decrease_moves()

    ''' Adds num to self.current buffer '''
    def add(self, button, num):
        self.current += num
        self.decrease_moves()

    ''' Subtracts num from self.current buffer '''
    def sub(self, button, num):
        self.current -= num
        self.decrease_moves()

    ''' Divides self.current buffer by num '''
    def divide(self, button, num):
        self.current = self.current / num
        self.decrease_moves()

    ''' Appends num to the end of self.current buffer '''
    def append(self, button, num):
        self.current = int(str(self.current + num))
        self.decrease_moves()

    ## Special actions - 
    # SUM, REPLACE, REVERSE, DELETE

    '''Returns sum of all integers in self.current'''
    def sum(self, button):
        sum = 0
        for i in self.current:
            sum += i
        self.current = sum
        self.decrease_moves()
    
    ''' Replaces `replace` in self.current with `replacer` '''
    def replace(self, button, replace, replacer):
        if str(replace) in str(self.current):
            string = str(self.current).replace(str(replace), str(replacer))
            self.current = int(string)
        self.decrease_moves()

    ''' Reverses the self.current number '''
    def reverse(self, button):
        length = len(str(self.current)) - 1
        rev = ''
        for x in range(length, -1, -1):
            rev += str(self.current)[x]
        self.current = int(rev)
        self.decrease_moves()

    ''' Deletes last index in self.current buffer '''
    def delete(self, button):
        ## We have to return the first x chars instead of removing the last
        new = ''
        for x in range(0, len(str(self.current)) - 1):
            new += str(self.current)[x]
        self.current = int(new)
        self.decrease_moves()

