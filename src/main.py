## Main UI initialization goes in here
import gi, os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from level import Level
import json
from pprint import pprint

class CalcWindow(Gtk.Window):
    def __init__(self):
        ## Main window init
        Gtk.Window.__init__(self, title="Calc")
        self.props.border_width=10

        self.set_default_size(45, 45)

        ##level_buttons = [{"method": "mult", "value": 9}, {"method": "add", "value": 1}, {"method": "blanknorm", "value": "N/A"}]
        ## Pull level data from levels file...
        self.pwd = os.path.dirname(os.path.realpath(__file__))
        with open(self.pwd +'/levels.json') as levels_file:
            json_levels = json.load(levels_file)
        
        self.levels = []
        for level in json_levels["levels"]:
            newlevel = Level(level["buttons"], level["moves"], level["goal"], level["starting"], self)
            self.levels.append(newlevel)
        self.level_index = 0
        self.load()
        self.level = self.levels[self.level_index]

        ## Main VBox init (Holds text box, labels and grid)
        self.vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.add(self.vbox)

        ## HBox for score/labels
        self.labelhbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=15)
        self.vbox.add(self.labelhbox)

        ## Score labels: Moves, Goal
        self.moves_label = Gtk.Label()
        self.goal_label = Gtk.Label()
        self.labelhbox.add(self.moves_label)
        self.labelhbox.add(self.goal_label)

        ## Text box
        self.entry = Gtk.Entry()
        self.vbox.add(self.entry)
        
        self.setup_vals()

        ## Grid to hold buttons
        self.buttons_grid = Gtk.Grid()
        self.vbox.add(self.buttons_grid)
        self.buttons_grid.set_column_spacing(10)
        self.buttons_grid.set_row_spacing(10)

        ## Buttons for grid
        button1 = Gtk.Button(label="1")
        button2 = Gtk.Button(label="2")
        button3 = Gtk.Button(label="3")
        button4 = Gtk.Button(label="4")
        button5 = Gtk.Button(label="5")
        button6 = Gtk.Button(label="6")
        button7 = Gtk.Button(label="7")
        button8 = Gtk.Button(label="8")
        button9 = Gtk.Button(label="9")

        ## We put the buttons into a list of vertical rows for easy operation later
        self.button_list = [[button1, button4, button7],
                            [button2, button5, button8],
                            [button3, button6, button9]]

        ## Add the buttons to their position on the grid
        self.buttons_grid.add(button1)
        self.buttons_grid.attach_next_to(button2, button1, Gtk.PositionType.RIGHT, 1, 1)
        self.buttons_grid.attach_next_to(button3, button2, Gtk.PositionType.RIGHT, 1, 1)
        self.buttons_grid.attach_next_to(button4, button1, Gtk.PositionType.BOTTOM, 1, 1)
        self.buttons_grid.attach_next_to(button5, button2, Gtk.PositionType.BOTTOM, 1, 1)
        self.buttons_grid.attach_next_to(button6, button3, Gtk.PositionType.BOTTOM, 1, 1)
        self.buttons_grid.attach_next_to(button7, button4, Gtk.PositionType.BOTTOM, 1, 1)
        self.buttons_grid.attach_next_to(button8, button5, Gtk.PositionType.BOTTOM, 1, 1)
        self.buttons_grid.attach_next_to(button9, button6, Gtk.PositionType.BOTTOM, 1, 1)

        self.setup_buttons()

    def level_init(self):
        self.setup_vals()
        self.setup_buttons()

    def setup_vals(self):
        ## Score labels: Moves, Goal
        self.moves_label.set_text("Moves: " +str(self.level.moves))
        self.goal_label.set_text("Goal: " +str(self.level.goal))

        ## Text box        
        self.entry.set_text(str(self.level.current))
        

    def setup_buttons(self):
        for index, button_widget in enumerate(self.button_list[0]):
            button = self.level.buttons[0][index]
            if not button['method'] == 'blank':
                button_widget.set_label("")
            else:
                button_widget.set_label("")
        for index, button_widget in enumerate(self.button_list[1]):
            button = self.level.buttons[1][index]
            if not button['method'] == 'blank':
                if button['method'] == 'mult':
                    button_widget.set_label('x' +str(button['value']))
                    button_widget.connect("clicked", self.level.mult, button['value'])
                if button['method'] == 'add':
                    button_widget.set_label('+' +str(button['value']))
                    button_widget.connect("clicked", self.level.add, button['value'])
                if button['method'] == 'sub':
                    button_widget.set_label('-' +str(button['value']))
                    button_widget.connect("clicked", self.level.sub, button['value'])
                if button['method'] == 'div':
                    button_widget.set_label('/' +str(button['value']))
                    button_widget.connect("clicked", self.level.divide, button['value'])
                if button['method'] == 'append':
                    button_widget.set_label('((' +str(button['value']) +'))')
                    button_widget.connect("clicked", self.level.append, button['value'])
            else:
                button_widget.set_label("")
        for index, button_widget in enumerate(self.button_list[2]):
            button = self.level.buttons[2][index]
            if not button['method'] == 'blank':
                if button['method'] == 'clr':
                    button_widget.set_label('CLR')
                    button_widget.connect("clicked", self.level.CLR)
                if button['method'] == 'sum':
                    button_widget.set_label('SUM')
                    button_widget.connect("clicked", self.level.sum)
                if button['method'] == 'replace':
                    replace = button['value'].split()[0]
                    replacer = button['value'].split()[1]
                    button_widget.set_label(str(replace) + ">>" +str(replacer))
                    button_widget.connect("clicked", self.level.replace, int(replace), int(replacer))
                if button['method'] == 'reverse':
                    button_widget.set_label('REV')
                    button_widget.connect("clicked", self.level.reverse)
                if button['method'] == 'delete':
                    button_widget.set_label('DEL')
                    button_widget.connect("clicked", self.level.delete)
            else:
                button_widget.set_label("")

    def update(self):
        self.entry.set_text(str(self.level.current))
        self.moves_label.set_text("Moves: " +str(self.level.moves))

    def win(self):
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.WARNING,
            Gtk.ButtonsType.OK_CANCEL, "Nice job!")
        dialog.format_secondary_text("Ready for the next level?")
        response = dialog.run()
        
        if response == Gtk.ResponseType.OK:
            self.level_index += 1
            self.save()
            self.level = self.levels[self.level_index]
            self.level_init()
            dialog.destroy()
        elif response == Gtk.ResponseType.CANCEL:        
            dialog.destroy()

    def lose(self):
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.INFO,
            Gtk.ButtonsType.OK, "That's not it!!")
        dialog.format_secondary_text("Try again!")
        dialog.run()
        self.save()
        dialog.destroy()
        self.level.CLR("button")

    def start(self):
        win = CalcWindow()
        win.connect("delete-event", Gtk.main_quit)
        win.show_all()
        Gtk.main()

    def save(self):
        data = {"level": self.level_index}
        with open(self.pwd +"/save.json", "w") as savefile:
            json.dump(data, savefile)

    def load(self):
        if not os.path.isfile(self.pwd +'/save.json'):
            return    
        else:
            with open(self.pwd +'/save.json') as save_file:
                save = json.load(save_file)
                self.level_index = save['level']