#!/bin/bash
###########################
## Calc installer script ##
## Installs calc and its ##
## files to /usr/bin/    ##
###########################

## Make sure we're su
if [ "$(id -u)" != "0" ]; then
   echo "This installer must be run as root"
   exit 1
fi

## Make install directory
mkdir /usr/bin/calc-files

## Move files
mv src/* /usr/bin/calc-files

## Make launcher in /usr/bin
mv calc /usr/bin/

## Make launcher executable
chmod +x /usr/bin/calc-files/calc
chmod +x /usr/bin/calc

## Install desktop launcher
mv calc.desktop /usr/share/applications/calc-game.desktop